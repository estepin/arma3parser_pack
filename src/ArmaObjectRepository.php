<?php
namespace Geekstart\Arma3parserPack;

class ArmaObjectRepository implements contracts\ArmaObjectRepository
{
    protected $filePath;

    function __construct($pathToResult)
    {
        $this->filePath = $pathToResult.'/data.txt';
    }

    /**
     * @return ArmaObject[]
     */

    function getAll() : array
    {
        if(!file_exists($this->filePath)) {
            return [];
        }

        $result = [];
        $stringData = file($this->filePath);
        foreach ($stringData as $str) {
            $data = json_decode($str, true);
            $result[] = $this->dataToObject($data);
        }

        return $result;
    }

    function save(ArmaObject $object)
    {
        $allObjects = $this->getAll();
        foreach ($allObjects as $k => $objectInFile) {
            if($objectInFile->getClassName() == $object->getClassName()) {
                $allObjects[$k] = $object;
            }
        }

        $this->saveAll($allObjects);
    }


    /**
     * @param ArmaObject[] $objects
     */
    function saveAll(array $objects)
    {
        $str = '';
        foreach ($objects as $object)
        {
            $str .= json_encode($this->objectToArray($object)).PHP_EOL;
        }

        file_put_contents($this->filePath, $str);
    }

    function clearRepository()
    {
        if(file_exists($this->filePath)) {
            unlink($this->filePath);
        }
    }

    protected function objectToArray(ArmaObject $object) : array
    {
        return [
            'className' => $object->getClassName(),
            'baseClassName' => $object->getBaseClassName(),
            'picturePath' => $object->getPicturePath(),
            'magazines' => $object->getMagazines(),
            'baseType' => $object->getBaseType(),
            'baseTypeTree' => $object->getBaseTypeTree(),
            'side' => $object->getSide(),
            'faction' => $object->getFaction(),
            'model' => $object->getModel(),
            'screenshots' => $object->getScreenshots(),
            'scope' => $object->getScope(),
            'fuelCapacity' => $object->getFuelCapacity(),
            'maxSpeed' => $object->getMaxSpeed(),
            'slingLoadMaxCargoMass' => $object->getSlingLoadMaxCargoMass(),
            'maximumLoad' => $object->getMaximumLoad(),
            'armor' => $object->getArmor(),
            'weapons' => $object->getWeapons(),
            'transportMaxMagazines' => $object->getTransportMaxMagazines(),
            'transportMaxWeapons' => $object->getTransportMaxWeapons(),
            'transportMaxBackpacks' => $object->getTransportMaxBackpacks(),
            'compatibleItems' => $object->getCompatibleItems()
        ];
    }

    protected function dataToObject($data) : ArmaObject
    {
        $object = new ArmaObject();
        $object->setClassName($data['className']);
        $object->setBaseClassName($data['baseClassName']);
        $object->setPicturePath($data['picturePath']);
        $object->setMagazines($data['magazines']);
        $object->setBaseType($data['baseType']);
        $object->setBaseTypeTree($data['baseTypeTree']);
        $object->setSide($data['side']);
        $object->setFaction($data['faction']);
        $object->setModel($data['model']);
        $object->setScreenshots($data['screenshots']);
        $object->setScope($data['scope']);
        $object->setFuelCapacity($data['fuelCapacity']);
        $object->setMaxSpeed($data['maxSpeed']);
        $object->setSlingLoadMaxCargoMass ($data['slingLoadMaxCargoMass']);
        $object->setMaximumLoad($data['maximumLoad']);
        $object->setArmor($data['armor']);
        $object->setWeapons($data['weapons']);
        $object->setTransportMaxMagazines($data['transportMaxMagazines']);
        $object->setTransportMaxWeapons($data['transportMaxWeapons']);
        $object->setTransportMaxBackpacks($data['transportMaxBackpacks']);
        $object->setCompatibleItems($data['compatibleItems']);

        return $object;
    }
}