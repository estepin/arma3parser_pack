<?php
namespace Geekstart\Arma3parserPack;

class ArmaObject
{
    protected $className;
    protected $baseClassName;
    protected $picturePath;
    protected $screenshots;
    protected $magazines;
    protected $generalMacro;
    protected $baseType;
    protected $baseTypeTree;
    protected $side;
    protected $faction;
    protected $editorPreview;
    protected $model;
    protected $scope;

    protected $maxSpeed;
    protected $fuelCapacity;
    protected $slingLoadMaxCargoMass;
    protected $maximumLoad;
    protected $armor;
    protected $weapons;
    protected $transportMaxMagazines;
    protected $transportMaxWeapons;
    protected $transportMaxBackpacks;

    protected $compatibleItems;

    /**
     * @return mixed
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param mixed $className
     */
    public function setClassName($className): void
    {
        $this->className = $className;
    }

    /**
     * @return mixed
     */
    public function getBaseClassName()
    {
        return $this->baseClassName;
    }

    /**
     * @param mixed $baseClassName
     */
    public function setBaseClassName($baseClassName): void
    {
        $this->baseClassName = $baseClassName;
    }

    /**
     * @return mixed
     */
    public function getPicturePath()
    {
        return $this->picturePath;
    }

    /**
     * @param mixed $picturePath
     */
    public function setPicturePath($picturePath): void
    {
        $this->picturePath = $picturePath;
    }

    /**
     * @return mixed
     */
    public function getMagazines()
    {
        return $this->magazines;
    }

    /**
     * @param mixed $magazines
     */
    public function setMagazines($magazines): void
    {
        $this->magazines = $magazines;
    }

    /**
     * @return mixed
     */
    public function getGeneralMacro()
    {
        return $this->generalMacro;
    }

    /**
     * @param mixed $generalMacro
     */
    public function setGeneralMacro($generalMacro): void
    {
        $this->generalMacro = $generalMacro;
    }

    /**
     * @return mixed
     */
    public function getBaseType()
    {
        return $this->baseType;
    }

    /**
     * @param mixed $baseType
     */
    public function setBaseType($baseType): void
    {
        $this->baseType = $baseType;
    }

    /**
     * @return mixed
     */
    public function getBaseTypeTree()
    {
        return $this->baseTypeTree;
    }

    /**
     * @param mixed $baseTypeTree
     */
    public function setBaseTypeTree($baseTypeTree): void
    {
        $this->baseTypeTree = $baseTypeTree;
    }

    /**
     * @return mixed
     */
    public function getSide()
    {
        return $this->side;
    }

    /**
     * @param mixed $side
     */
    public function setSide($side): void
    {
        $this->side = $side;
    }

    /**
     * @return mixed
     */
    public function getFaction()
    {
        return $this->faction;
    }

    /**
     * @param mixed $faction
     */
    public function setFaction($faction): void
    {
        $this->faction = $faction;
    }

    /**
     * @return mixed
     */
    public function getEditorPreview()
    {
        return $this->editorPreview;
    }

    /**
     * @param mixed $editorPreview
     */
    public function setEditorPreview($editorPreview): void
    {
        $this->editorPreview = $editorPreview;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     * @return ArmaObject
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScreenshots()
    {
        return $this->screenshots;
    }

    /**
     * @param mixed $screenshots
     */
    public function setScreenshots($screenshots): void
    {
        $this->screenshots = $screenshots;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     */
    public function setScope($scope): void
    {
        $this->scope = $scope;
    }

    /**
     * @return mixed
     */
    public function getMaxSpeed()
    {
        return $this->maxSpeed;
    }

    /**
     * @param mixed $maxSpeed
     */
    public function setMaxSpeed($maxSpeed): void
    {
        $this->maxSpeed = $maxSpeed;
    }

    /**
     * @return mixed
     */
    public function getFuelCapacity()
    {
        return $this->fuelCapacity;
    }

    /**
     * @param mixed $fuelCapacity
     */
    public function setFuelCapacity($fuelCapacity): void
    {
        $this->fuelCapacity = $fuelCapacity;
    }

    /**
     * @return mixed
     */
    public function getSlingLoadMaxCargoMass()
    {
        return $this->slingLoadMaxCargoMass;
    }

    /**
     * @param mixed $slingLoadMaxCargoMass
     */
    public function setSlingLoadMaxCargoMass($slingLoadMaxCargoMass): void
    {
        $this->slingLoadMaxCargoMass = $slingLoadMaxCargoMass;
    }

    /**
     * @return mixed
     */
    public function getMaximumLoad()
    {
        return $this->maximumLoad;
    }

    /**
     * @param mixed $maximumLoad
     */
    public function setMaximumLoad($maximumLoad): void
    {
        $this->maximumLoad = $maximumLoad;
    }

    /**
     * @return mixed
     */
    public function getArmor()
    {
        return $this->armor;
    }

    /**
     * @param mixed $armor
     */
    public function setArmor($armor): void
    {
        $this->armor = $armor;
    }

    /**
     * @return mixed
     */
    public function getWeapons()
    {
        return $this->weapons;
    }

    /**
     * @param mixed $weapons
     */
    public function setWeapons($weapons): void
    {
        $this->weapons = $weapons;
    }

    /**
     * @return mixed
     */
    public function getTransportMaxMagazines()
    {
        return $this->transportMaxMagazines;
    }

    /**
     * @param mixed $transportMaxMagazines
     */
    public function setTransportMaxMagazines($transportMaxMagazines): void
    {
        $this->transportMaxMagazines = $transportMaxMagazines;
    }

    /**
     * @return mixed
     */
    public function getTransportMaxWeapons()
    {
        return $this->transportMaxWeapons;
    }

    /**
     * @param mixed $transportMaxWeapons
     */
    public function setTransportMaxWeapons($transportMaxWeapons): void
    {
        $this->transportMaxWeapons = $transportMaxWeapons;
    }

    /**
     * @return mixed
     */
    public function getTransportMaxBackpacks()
    {
        return $this->transportMaxBackpacks;
    }

    /**
     * @param mixed $transportMaxBackpacks
     */
    public function setTransportMaxBackpacks($transportMaxBackpacks): void
    {
        $this->transportMaxBackpacks = $transportMaxBackpacks;
    }

    /**
     * @return mixed
     */
    public function getCompatibleItems()
    {
        return $this->compatibleItems;
    }

    /**
     * @param mixed $compatibleItems
     */
    public function setCompatibleItems($compatibleItems): void
    {
        $this->compatibleItems = $compatibleItems;
    }

}