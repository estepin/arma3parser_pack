<?php


namespace Geekstart\Arma3parserPack\contracts;


use Geekstart\Arma3parserPack\ArmaObject;

interface ArmaObjectRepository
{
    /**
     * @return ArmaObject[]
     */

    function getAll() : array;

    function save(ArmaObject $object);


    /**
     * @param ArmaObject[] $objects
     */
    function saveAll(array $objects);

    function clearRepository();
}